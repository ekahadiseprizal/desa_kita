<?php

namespace App\Http\Controllers\controller_public_desa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contact(){
        return view('layouts_public_desaku.konten.contact');
    }
}
