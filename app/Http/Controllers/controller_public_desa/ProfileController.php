<?php

namespace App\Http\Controllers\controller_public_desa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function profile(){
        return view('layouts_public_desaku.konten.profile');
    }
}
