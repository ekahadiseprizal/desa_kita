<?php

namespace App\Http\Controllers\controller_public_desa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function home(){
        return view('layouts_public_desaku.konten.home');
    }

    public function cari(){
        return view('layouts_public_desaku.konten.search');
    }
}
