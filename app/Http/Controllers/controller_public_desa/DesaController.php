<?php

namespace App\Http\Controllers\controller_public_desa;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DesaController extends Controller
{
    public function dana_desa(){
        return view('layouts_public_desaku.konten.dana_desa');
    }
}
