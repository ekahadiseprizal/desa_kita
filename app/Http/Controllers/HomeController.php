<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;

class HomeController extends Controller
{
    public function index(){
        // dd(Auth::check());
        return view('home.index');
    }

    public function home(){
        return view('home.home');
    }
}


