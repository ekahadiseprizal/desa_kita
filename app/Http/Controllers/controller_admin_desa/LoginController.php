<?php

namespace App\Http\Controllers\controller_admin_desa;

use Illuminate\Http\Request;
use App\User;
use Hash;
use App\Role;
use Auth;

class LoginController extends Controller
{
    public function register()
    {
        $users = User::get();
        return view ('layouts_admin_desa.login.register');
    }
    public function post_register(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->contact = $request->contact;
        $user->password = Hash::make($request->password);
        $user->roles_id = 2;
        $user->save();
        return view('layout_admin_desa.login.login');
    }

    public function login()
    {
        return view('layout_admin_desa.login.login');
    }

    public function post_login(Request $request){
        

        $email = $request->email;
        $password = $request->password;

        $data = User::where('email',$email)->first();
        if($data){ 
            if(Hash::check($password,$data->password)){
                
                Auth::login($data);
                return redirect('index');
            }
            else{
                return redirect('login')->with('error','Password atau Email, Salah !');
                
            }
        }
        else{
            return redirect('login')->with('error','Password atau Email, Salah!');
        }
    }

    public function logout(Request $request)
    {
            $this->guard()->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();

            return $this->loggedOut($request) ?: redirect('/login');
       
    }
    protected function loggedOut(Request $request)
    {
        //
    }

    protected function guard()
    {
        return Auth::guard();
    }
}

