<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// WEB PUBLIC DESA
Route::get('/','controller_public_desa\HomeController@home');
Route::get('/cari','controller_public_desa\HomeController@cari');
Route::get('/profile','controller_public_desa\ProfileController@profile');
Route::get('/dana_desa','controller_public_desa\DesaController@dana_desa');
Route::get('/contact','controller_public_desa\ContactController@contact');
Route::get('/register', 'LoginController@register');
Route::post('/register', 'LoginController@post_register');
Route::get('/login', 'LoginController@login')->name('login');
Route::post('/login', 'LoginController@post_login');
Route::post('/logout', 'LoginController@logout');


Route::group(['middleware' => ['auth']], function () {   
Route::get('/index','HomeController@index');
}); 

// WEB ADMIN DESA

