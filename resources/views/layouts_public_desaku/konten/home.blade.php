<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Desa Kita </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets_desa_public/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="assets_desa_public/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/slicknav.css">
    <link rel="stylesheet" href="assets_desa_public/css/flaticon.css">
    <link rel="stylesheet" href="assets_desa_public/css/gijgo.css">
	<link rel="stylesheet" href="assets_desa_public/css/animate.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/magnific-popup.css">
	<link rel="stylesheet" href="assets_desa_public/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/themify-icons.css">
	<link rel="stylesheet" href="assets_desa_public/css/slick.css">
	<link rel="stylesheet" href="assets_desa_public/css/nice-select.css">
	<link rel="stylesheet" href="assets_desa_public/css/style.css">
</head>
<body>
    <!--? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="assets_desa_public/img/logo/loder.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    <div class="slider-area ">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height d-flex align-items-center">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-xl-6 col-lg-7 col-md-8">
                                <div class="hero__caption">
                                
                                <h2 data-animation="fadeInLeft" data-delay=".4s">Desa Kita</h2>
                                    <h1 data-animation="fadeInLeft" data-delay=".4s">Always Transparent !</h1>
                                    <!-- Hero-btn -->
                                    
                                    <div class="hero__btn">
                                        <a href="/cari" class="btn hero-btn"  data-animation="fadeInLeft" data-delay=".8s">Cari Desa</a>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-xl-6 col-lg-5">
                                <div class="hero-man d-none d-lg-block f-right" data-animation="jello" data-delay=".4s">
                                    <img src="assets_desa_public/img/hero/heroman.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            
</div>
    

    <!-- JS here -->

    <script src="assets_desa_public/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="assets_desa_public/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="assets_desa_public/js/popper.min.js"></script>
    <script src="assets_desa_public/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="assets_desa_public/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="assets_desa_public/js/owl.carousel.min.js"></script>
    <script src="assets_desa_public/js/slick.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="assets_desa_public/js/wow.min.js"></script>
    <script src="assets_desa_public/js/animated.headline.js"></script>
    <script src="assets_desa_public/js/jquery.magnific-popup.js"></script>

    <!-- Date Picker -->
    <script src="assets_desa_public/js/gijgo.min.js"></script>
    <!-- Nice-select, sticky -->
    <script src="assets_desa_public/js/jquery.nice-select.min.js"></script>
    <script src="assets_desa_public/js/jquery.sticky.js"></script>
    
    <!-- counter , waypoint -->
    <script src="assets_desa_public/js/jquery.counterup.min.js"></script>
    <script src="assets_desa_public/js/waypoints.min.js"></script>
    
    <!-- contact js -->
    <script src="assets_desa_public/js/contact.js"></script>
    <script src="assets_desa_public/js/jquery.form.js"></script>
    <script src="assets_desa_public/js/jquery.validate.min.js"></script>
    <script src="assets_desa_public/js/mail-script.js"></script>
    <script src="assets_desa_public/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="assets_desa_public/js/plugins.js"></script>
    <script src="assets_desa_public/js/main.js"></script>
    
    </body>
</html>


<!-- <a href="/login" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">login</a>
  <a href="/register" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">register</a> -->
