@extends('layouts_public_desaku.layouts.master')
@section('content')

<style>
    .slider-height2 {
    background-image: url(../img/hero/hero2.png);
    min-height: 300px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
}
</style>

<div class="slider-area ">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Selamat datang di Desa Durian</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

<div class="team-area pt-100 pb-100">
            <div class="container">
                <div class="row justify-content-sm-center">
                        <div class="cl-xl-7 col-lg-8 col-md-10">
                            <!-- Section Tittle -->
                            <div class="section-tittle text-center mb-70">
                            
                                <h2>Penerimaan APBDes</h2>
                            </div> 
                        </div>
                </div>
                <div class="row">
                <table class="table table-hover">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Jumlah Dana</th>
      <th scope="col">Kategori</th>
      <th scope="col">Tanggal</th>
      
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>Mark</td>
      <td>Otto</td>
      <td>@mdo</td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Jacob</td>
      <td>Thornton</td>
      <td>@fat</td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td colspan="2">Larry the Bird</td>
      <td>@twitter</td>
    </tr>
  </tbody>
</table>
                    
                </div>
            </div>
</div>

@endsection