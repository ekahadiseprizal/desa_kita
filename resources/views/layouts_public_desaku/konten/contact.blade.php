@extends('layouts_public_desaku.layouts.master')
@section('content')
<style>
    .slider-height2 {
    background-image: url(../img/hero/hero2.png);
    min-height: 300px;
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
}
</style>
<div class="slider-area ">
            <div class="slider-height2 d-flex align-items-center">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="hero-cap hero-cap2 text-center">
                                <h2>Selamat datang di Desa</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
</div>

<section class="contact-section">
            <div class="container">
                <div class="d-none d-sm-block mb-5 pb-4">
                    <div class="map">
                        <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.79817076149!2d107.65855851431704!3d-6.914718169596521!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e7f08d5629e3%3A0xcc3e45d8d380b235!2sJl.%20Kuningan%20XIV%20No.2kel%2C%20Antapani%20Tengah%2C%20Kec.%20Antapani%2C%20Kota%20Bandung%2C%20Jawa%20Barat%2040291!5e0!3m2!1sid!2sid!4v1594369441631!5m2!1sid!2sid"
                            height="500" width='100%' style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                    <div class="my-5"></div>
</div>
</div>
</div>
@endsection