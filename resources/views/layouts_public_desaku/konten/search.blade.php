@extends('layouts_public_desaku.layouts.master')
@section('content')

<style>
    body{
  margin: 0;
  padding: 0;
  font-family: "Montserrat";
}
.searchbox{
  width: 100%;
  margin: 10px auto;
}
.header{
  background: #2d3092;
  overflow: hidden;
  padding: 20px;
  text-align: center;
}
.header h1{
  text-transform: uppercase;
  color: white;
  margin: 0;
  margin-bottom: 8px;
}
#value{
  border: none;
  background: #f4f4f4;
  padding: 6px;
  font-size: 18px;
  width: 80%;
  border-radius: 6px;
  color: #212529;
}


.profile{
  margin: 6px 0;
  display: flex;
  align-items: center;
}

.icon{
  width: 40px;
  height: 40px;
  background: #2d3092;
  color: white;
  font-size: 24px;
  text-align: center;
  line-height: 40px;
  border-radius: 50%;
  margin-right: 8px;
}
.name{
  font-size: 18px;
  font-weight: 600;
  color: #333;
}


</style>


<div class="searchbox">
  <div class="header">
    <h1>Cari Desa</h1>
    <input onkeyup="filter()" type="text" id="value" placeholder="masukan nama desa">
  </div>

  <div class="container">
    <div class="profile">
      <span class="icon">A</span>
      <span class="name">Archie Hansen</span>
    </div>

    <div class="profile">
      <span class="icon">C</span>
      <span class="name">Celia Wood</span>
    </div>

    <div class="profile">
      <span class="icon">D</span>
      <span class="name">Danilelle Spencer</span>
    </div>

    <div class="profile">
      <span class="icon">M</span>
      <span class="name">Margarita Austin</span>
    </div>

    <div class="profile">
      <span class="icon">S</span>
      <span class="name">Simon Daniels</span>
    </div>

    <div class="profile">
      <span class="icon">G</span>
      <span class="name">Glenn Stevenson</span>
    </div>
    <div class="profile">
      <span class="icon">C</span>
      <span class="name">Collen black</span>
    </div>
    <div class="profile">
      <span class="icon">M</span>
      <span class="name">Miriam Johnston</span>
    </div>
  </div>
</div>


<script type="text/javascript">
  function filter() {

    var value,name,profile,i;

    value = document.getElementById("value").value.toUpperCase();
    profile = document.getElementsByClassName("profile");

    for(i=0;i<profile.length;i++){
      name = profile[i].getElementsByClassName("name");
      if (name[0].innerHTML.toUpperCase().indexOf(value) > -1) {
        profile[i].style.display = "flex";
      }else{
        profile[i].style.display = "none";
      }
    }

  }
</script>




@endsection