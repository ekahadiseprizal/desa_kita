<header>

<style>
    /* img {
    vertical-align: middle;
    border-style: none;
    width: 93px;
} */

.header-area .header-bottom .logo {
    padding: 4px 21px;
    margin-right: 73px;
    background: #ffffff;
}


</style>
        <!-- Header Start -->
        <div class="header-area">
            <div class="main-header">
                <div class="header-top d-none d-lg-block">
                    
                    <div class="container">
                        <div class="col-xl-12">
                            <div class="row d-flex justify-content-between align-items-center">
                                <div class="header-info-left">
                                    <ul>     
                                        <li>needhelp@gmail.com</li>
                                        <li>666 7475 25252</li>
                                    </ul>
                                </div>
                                <div class="header-info-right">
                                    <ul>    
                                        <li><a href="#"><i class="ti-user"></i>Login</a></li>
                                        <li><a href="#"><i class="ti-lock"></i>Register</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="header-bottom header-sticky">
                    <!-- Logo -->
                    <div class="logo d-none d-lg-block ">
                        <a href="index.html"><img src="assets_desa_public/img/logo/logon.png" height="94px" width="94px"></a>
                    </div>
                    <div class="container">
                        <div class="menu-wrapper">
                            <!-- Logo -->
                            
                            <!-- Main-menu -->
                            <div class="main-menu d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">                                                                                          
                                        <li><a href="/">Home</a></li>
                                        <li><a href="/profile">Profile</a></li>
                                        <li><a href="/dana_desa">APBDes</a></li>
                                        <li><a href="/kegiatan">Kegiatan</a></li>
                                        <li><a href="/contact">Contact</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <!-- Header-btn -->
                            <div class="header-search d-none d-lg-block">
                                <form action="#" class="form-box f-right ">
                                    <input type="text" name="Search" placeholder="Search Courses">
                                    <div class="search-icon">
                                        <i class="fas fa-search special-tag"></i>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Header End -->
    </header>