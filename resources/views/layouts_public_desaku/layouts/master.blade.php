<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> Desa Kita </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="assets_desa_public/img/favicon.ico">

	<!-- CSS here -->
	<link rel="stylesheet" href="assets_desa_public/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/owl.carousel.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/slicknav.css">
    <link rel="stylesheet" href="assets_desa_public/css/flaticon.css">
    <link rel="stylesheet" href="assets_desa_public/css/gijgo.css">
	<link rel="stylesheet" href="assets_desa_public/css/animate.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/magnific-popup.css">
	<link rel="stylesheet" href="assets_desa_public/css/fontawesome-all.min.css">
	<link rel="stylesheet" href="assets_desa_public/css/themify-icons.css">
	<link rel="stylesheet" href="assets_desa_public/css/slick.css">
	<link rel="stylesheet" href="assets_desa_public/css/nice-select.css">
	<link rel="stylesheet" href="assets_desa_public/css/style.css">
</head>
<body>
    <!--? Preloader Start -->
    <div id="preloader-active">
        <div class="preloader d-flex align-items-center justify-content-center">
            <div class="preloader-inner position-relative">
                <div class="preloader-circle"></div>
                <div class="preloader-img pere-text">
                    <img src="assets_desa_public/img/logo/loder.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- Preloader Start -->
    @include('layouts_public_desaku.layouts.navbar')

    @yield('content')

    @include('layouts_public_desaku.layouts.footer')
    

    <!-- JS here -->

    <script src="assets_desa_public/js/vendor/modernizr-3.5.0.min.js"></script>
    <!-- Jquery, Popper, Bootstrap -->
    <script src="assets_desa_public/js/vendor/jquery-1.12.4.min.js"></script>
    <script src="assets_desa_public/js/popper.min.js"></script>
    <script src="assets_desa_public/js/bootstrap.min.js"></script>
    <!-- Jquery Mobile Menu -->
    <script src="assets_desa_public/js/jquery.slicknav.min.js"></script>

    <!-- Jquery Slick , Owl-Carousel Plugins -->
    <script src="assets_desa_public/js/owl.carousel.min.js"></script>
    <script src="assets_desa_public/js/slick.min.js"></script>
    <!-- One Page, Animated-HeadLin -->
    <script src="assets_desa_public/js/wow.min.js"></script>
    <script src="assets_desa_public/js/animated.headline.js"></script>
    <script src="assets_desa_public/js/jquery.magnific-popup.js"></script>

    <!-- Date Picker -->
    <script src="assets_desa_public/js/gijgo.min.js"></script>
    <!-- Nice-select, sticky -->
    <script src="assets_desa_public/js/jquery.nice-select.min.js"></script>
    <script src="assets_desa_public/js/jquery.sticky.js"></script>
    
    <!-- counter , waypoint -->
    <script src="assets_desa_public/js/jquery.counterup.min.js"></script>
    <script src="assets_desa_public/js/waypoints.min.js"></script>
    
    <!-- contact js -->
    <script src="assets_desa_public/js/contact.js"></script>
    <script src="assets_desa_public/js/jquery.form.js"></script>
    <script src="assets_desa_public/js/jquery.validate.min.js"></script>
    <script src="assets_desa_public/js/mail-script.js"></script>
    <script src="assets_desa_public/js/jquery.ajaxchimp.min.js"></script>
    
    <!-- Jquery Plugins, main Jquery -->	
    <script src="assets_desa_public/js/plugins.js"></script>
    <script src="assets_desa_public/js/main.js"></script>
    
    </body>
</html>